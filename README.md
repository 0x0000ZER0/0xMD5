# 0xMD5

A `md5` implementation in `C`.

### API

- For hashing you can use the `z0_md5_hash` function:

```c
uint8_t txt[] = "abcd";
uint8_t hash[16];

z0_md5_hash(txt, sizeof (txt) - 1, hash);
```

- For printing the hash you can use the `z0_md5_print` function:

```c
uint8_t hash[16];

//...

z0_md5_print(hash);
```
