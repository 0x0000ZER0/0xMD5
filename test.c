#include "z0_md5.h"
#include "z0_test.h"

#include <string.h>

void
test_000() {
	Z0_TEST_NAME("TEST 000");

	u8 txt[] = "";
	u8 hash[16];
	z0_md5_hash(txt, sizeof (txt) - 1, hash);
	z0_md5_print(hash);

	u8 exp[16] = {
		0xD4, 0x1D, 0x8C, 0xD9,
		0x8F, 0x00, 0xB2, 0x04,
		0xE9, 0x80, 0x09, 0x98,
		0xEC, 0xF8, 0x42, 0x7E
	};

	int res;
	res = strncmp((char*)hash, (char*)exp, sizeof (hash));

	Z0_TEST_ASSERT(res == 0);
}

void
test_001() {
	Z0_TEST_NAME("TEST 001");

	u8 txt[] = "abcd";
	u8 hash[16];
	z0_md5_hash(txt, sizeof (txt) - 1, hash);
	z0_md5_print(hash);

	u8 exp[16] = {
		0xE2, 0xFC, 0x71, 0x4C,
		0x47, 0x27, 0xEE, 0x93,
		0x95, 0xF3, 0x24, 0xCD,
		0x2E, 0x7F, 0x33, 0x1F
	};

	int res;
	res = strncmp((char*)hash, (char*)exp, sizeof (hash));

	Z0_TEST_ASSERT(res == 0);
}

void
test_002() {
	Z0_TEST_NAME("TEST 002");
	
	u8 txt[] = "012345";
	u8 hash[16];
	z0_md5_hash(txt, sizeof (txt) - 1, hash);
	z0_md5_print(hash);

	u8 exp[16] = {
		0xD6, 0xA9, 0xA9, 0x33,
		0xC8, 0xAA, 0xFC, 0x51,
		0xE5, 0x5A, 0xC0, 0x66,
		0x2B, 0x6E, 0x4D, 0x4A
	};

	int res;
	res = strncmp((char*)hash, (char*)exp, sizeof (hash));

	Z0_TEST_ASSERT(res == 0);
}

void
test_003() {
	Z0_TEST_NAME("TEST 003");
	
	u8 txt[] = "'a','b','c','d','e','f','g','h','i','j','k','l','m','n'"
		",'o','p','q','r','s','t','u','v','w','x','y','z','A','B','C','D',"
		"'E','F','G','H','I','J','K','L','M','N','O','P','Q','R'"
		",'S','T','U','V','W','X','Y','Z'";
	u8 hash[16];
	z0_md5_hash(txt, sizeof (txt) - 1, hash);
	z0_md5_print(hash);

	u8 exp[16] = {
		0xB9, 0x25, 0x9F, 0x64,
		0x5D, 0x09, 0xB9, 0xE7,
		0x6D, 0xC1, 0x4A, 0x8F,
		0x5A, 0x55, 0xB4, 0x60
	};

	int res;
	res = strncmp((char*)hash, (char*)exp, sizeof (hash));

	Z0_TEST_ASSERT(res == 0);
}

int
main() {
	test_000();
	test_001();
	test_002();
	test_003();
	
	return 0;
}
