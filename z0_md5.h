#ifndef Z0_MD5_H
#define Z0_MD5_H

#include <stdint.h>

typedef uint8_t  u8;
typedef uint32_t u32;
typedef uint64_t u64;

void
z0_md5_hash(u8*, u64, u8*);

void
z0_md5_print(u8*);

#endif
