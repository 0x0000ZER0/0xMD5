
test: test.c
	gcc -O2 -Wall -pedantic test.c z0_md5.c -I0xTEST -o 0xMD5_test

debug:
	gcc -g -Wall -pedantic test.c z0_md5.c -I0xTEST -o 0xMD5_test -DZ0_DEBUG
