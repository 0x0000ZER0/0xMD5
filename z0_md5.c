#include "z0_md5.h"

#include <string.h>
#include <stdio.h>
#include <endian.h>

#ifdef Z0_DEBUG
static void
z0_dbg_blk_print(uint32_t *b, size_t l) {
        printf("========== Z0 DEBUG ==========\n");
        for (int t = 0; t < l; t += 2)
                printf("%08x %08x\n", b[t], b[t + 1]);
}
#endif

#define Z0_MD5_PAD  0
#define Z0_MD5_NONE 1
#define Z0_MD5_END  2

void
z0_md5_hash(u8 *msg, u64 len, u8 *hash) {
	static uint32_t K[] = {
		0xD76AA478,	0xE8C7B756,	0x242070DB,	0xC1BDCEEE,
		0xF57C0FAF,	0x4787C62A,	0xA8304613,	0xFD469501,
		0x698098D8,	0x8B44F7AF,	0xFFFF5BB1,	0x895CD7BE,
		0x6B901122,	0xFD987193,	0xA679438E,	0x49B40821,
		0xF61E2562,	0xC040B340,	0x265E5A51,	0xE9B6C7AA,
		0xD62F105D,	0x02441453,	0xD8A1E681,	0xE7D3FBC8,
		0x21E1CDE6,	0xC33707D6,	0xF4D50D87,	0x455A14ED,
		0xA9E3E905,	0xFCEFA3F8,	0x676F02D9,	0x8D2A4C8A,
		0xFFFA3942,	0x8771F681,	0x6D9D6122,	0xFDE5380C,
		0xA4BEEA44,	0x4BDECFA9,	0xF6BB4B60,	0xBEBFBC70,
		0x289B7EC6,	0xEAA127FA,	0xD4EF3085,	0x04881D05,
		0xD9D4D039,	0xE6DB99E5,	0x1FA27CF8,	0xC4AC5665,
		0xF4292244,	0x432AFF97,	0xAB9423A7,	0xFC93A039,
		0x655B59C3,	0x8F0CCC92,	0xFFEFF47D,	0x85845DD1,
		0x6FA87E4F,	0xFE2CE6E0,	0xA3014314,	0x4E0811A1,
		0xF7537E82,	0xBD3AF235,	0x2AD7D2BB,	0xEB86D391
	};

	static u32 R[] = {
		7,	12,	17,	22,  
		7,	12,	17,	22,  
		7,	12,	17,	22,  
		7,	12,	17,	22,

		5,	9,	14,	20,  
		5,	9,	14,	20,  
		5,	9,	14,	20,  
		5,	9,	14,	20,

		4,	11,	16,	23, 
		4,	11,	16,	23, 
		4,	11,	16,	23, 
		4,	11,	16,	23,

		6,	10,	15,	21, 
		6,	10,	15,	21, 
		6,	10,	15,	21, 
		6,	10,	15,	21
	};

	u32 H[] = {
		0x67452301,
		0xEFCDAB89,
		0x98BADCFE,
		0x10325476
	};

	u32 A;
	u32 B;
	u32 C;
	u32 D;

	u32 F;
	u32 g;

	u32 M[16];

	u64 off;
	off = len;

	u8 state;
	state = Z0_MD5_NONE;
	
	while (state != Z0_MD5_END) {
		if (off >= 64) {
			memcpy(M, msg, sizeof (M));				
			
			off -= 64;
			msg += 64;
		} else if (off >= 56) {
			memcpy(M, msg, off);
			*((u8*)M + off) = 0x80; // 0b 1000 0000
			memset((u8*)M + off + 1, 0, sizeof (M) - off - 1);

			state = Z0_MD5_PAD;
		} else {
			memcpy(M, msg, off);

			if (state != Z0_MD5_PAD)
				*((u8*)M + off) = 0x80; // 0b 1000 0000
		
			// reusing [state] variable: 
			// note that the state values are defined in such way,
			// that when NO PAD was performed then it adds one to the offset
			// for the 1 bit, but when there was added the bit in previous
			// step then the state becomes zero and (... + state) expression
			// evaluates to (...).
			memset((u8*)M + off + state, 0, sizeof (M) - off - 1 - sizeof (u64));	

			len *= 8;			
#if __BYTE_ORDER == __BIG_ENDIAN
			len = __builtin_bswap64(len);
#endif
			memcpy(M + 14, &len, sizeof (u64));

			state = Z0_MD5_END;
		}

#if __BYTE_ORDER == __BIG_ENDIAN
		for (u32 i = 0; i < 16; ++i)
			M[i] = __builtin_bswap32(M[i]);
#endif

#ifdef Z0_DEBUG
		z0_dbg_blk_print(M, 16);				
#endif

		A = H[0];
		B = H[1];
		C = H[2];
		D = H[3];


		for (u32 i = 0; i < 64; ++i) {
			if (i <= 15) {
				F = (B & C) | ((~B) & D);
				g = i;
			} else if (i <= 31) {
				F = (D & B) | ((~D) & C);
				g = (5 * i + 1) % 16;
			} else if (i <= 47) {
				F = B ^ C ^ D;
				g = (3 * i + 5) % 16;
			} else {
				F = C ^ (B | (~D));
				g = (7 * i) % 16;
			}			

			F = F + A + K[i] + M[g];
			A = D;
			D = C;
			C = B;
			B = B + ((F << R[i]) | (F >> (32 - R[i])));
		}

#ifdef Z0_DEBUG
		printf("A: %u\n", A);
		printf("D: %u\n", D);
		printf("C: %u\n", C);
		printf("B: %u\n", B);
#endif

		H[0] += A;
		H[1] += B;
		H[2] += C;
		H[3] += D;

#ifdef Z0_DEBUG
		printf("H[0]: %u\n", H[0]);
		printf("H[1]: %u\n", H[1]);
		printf("H[2]: %u\n", H[2]);
		printf("H[3]: %u\n", H[3]);
#endif
	} 

	memcpy(hash, H, sizeof (H));
}

void
z0_md5_print(u8 *hash) {
	printf("Z0 INFO: ");

	for (int i = 0; i < 16; ++i) {
		u32 dig;
		dig = *(u8*)(hash + i);

		printf("%02X", dig);
	}

	printf("\n");

}


